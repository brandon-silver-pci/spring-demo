package com.example.demo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import org.springframework.stereotype.Repository;

@Repository
public class PersonRepository implements CrudRepository<Person,String> {

    List<Person> personList = Arrays.asList(
            new Person("mike@email.com","Mike","1-800-AWESOME"),
            new Person("dave@email.com","Dave","1-800-AWESOME"),
            new Person("john@email.com","John","1-800-AWESOME"));

    @Override
    public Person save(Person model) {
        personList.add(model);
        return model;
    }

    @Override
    public void delete(Person model) {
        personList.remove(model);
    }

    @Override
    public void deleteById(String id) {
        personList.remove(Integer.parseInt(id));
    }

    @Override
    public Iterable<Person> findAll() {
        return personList;
    }

    @Override
    public Person findById(String id) {
        return personList
                .stream()
                .filter(p -> p.getEmail().equals(id))
                .findFirst()
                .orElse(null);
    }
}
