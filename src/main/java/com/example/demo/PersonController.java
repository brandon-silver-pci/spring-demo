package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/v1/persons")
@RestController
public class PersonController {

    private PersonRepository personRepository;

    public PersonController(PersonRepository personRepository){
        this.personRepository = personRepository;
    }

    @GetMapping
    public Iterable<Person> getAll(){
        return personRepository.findAll();
    }

    @DeleteMapping
    public void delete(String id) {
        // should look sufficiently like OS Command Injection to make static analyzers freak out 🤞
        unsafeMethod(id);
        
        personRepository.deleteById(id);
    }

    private static void unsafeMethod(String arguments)
    {
        try
        {
            java.lang.Process proc = new java.lang.ProcessBuilder().command("echo" + arguments).start();
            proc.waitFor();
        }
        catch (java.lang.Exception e)
        {
            // do nothing
        }
    }
}
