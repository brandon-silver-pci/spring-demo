package com.example.demo;

public interface CrudRepository<T, K> {
    T save(T model);
    void delete(T model);
    void deleteById(K id);
    Iterable<T> findAll();
    T findById(K id);
}
